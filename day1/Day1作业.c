#include<stdlib.h>
#include<stdio.h>
#define N 5
#define M 9
void ninetimesnine() {
	int i, j;
	for (i = 1; i <= 9; i++) {
		for (j = 1; j <= i; j++) {
			printf("%d*%d=%-2d ", j, i, i*j);
		}printf("\n");
	}
}

void printdiamond1() {
	int i,j;
	for (i = 1; i <= M; i++) {
		for (j = 1; j <= M; j++) {
			if (abs(j - N) <= min(i, 10 - i) - 1) {
					if ((j - (N - (min(i,10-i) - 1))) % 2 == 0) {
						printf("*");
					}
					else{
						printf(" ");
					}
			}
			else{ printf(" "); }
		}printf("\n");
	}
}

void printdiamond2() {
	int i, j;
	for (i = 1; i <= M; i++) {
		for (j = 1; j <= M; j++) {
			if (abs(j - N) == min(i, 10 - i) - 1) {
					printf("*");
				}
				else {
					printf(" ");
				}
		}printf("\n");
	}
}


int main() {
	//ninetimesnine();
	//printdiamond1();
	printdiamond2();
}