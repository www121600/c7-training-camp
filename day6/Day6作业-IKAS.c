#define _CRT_SECURE_NO_WARNINGS
#include<stdlib.h>
#include<stdio.h>
#include<string.h>

int step(int n) {
	if (n == 1) {
		return 1;
	}
	else if(n==2)
	{
		return 2;
	}
	else
	{
		return step(n - 1) + step(n - 2);
	}
}

int way(int m,int n) {
	if (n == 0) {
		return 1;
	}
	else if (m==0)
	{
		return 1;
	}
	else { 
		return (way(m, n - 1) + way(m - 1, n));
	}
}


int main() {
	/*int n;
	printf("请输入楼梯阶数：");
	scanf("%d",&n);
	printf("\n");
	int m=step(n);
	printf("共有%2d种上楼梯的方法\n",m);*/
	int m, n;
	printf("请输入目的横纵坐标:");
	scanf("%d", &m);
	scanf("%d", &n);
	int p=way(m, n);
	printf("共有%d种方式", p);
}