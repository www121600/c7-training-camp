#define _CRT_SECURE_NO_WARNINGS
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
//
//1.将包含字符数字的字符串分开, 使得分开后的字符串前一部分是数字后一部分是字母。例
//如“h1ell2o3”->”123hello”
//思路：遇到字母可以放入一个数组，数字可以放到另外一个数组，然后最后把两个数组拼起来
#define len_default 20
void distribution() {
	printf("请输入字符串：");
	char *p;
	char s[len_default];
	scanf("%s", s);
	p = (char*)malloc(strlen(s)+1);
	strcpy(p, s);
	//puts(p);
	int i;
	char alp[len_default], num[len_default];
	int count_alp = 0, count_num = 0;
	for (i = 0; i <= strlen(p); i++) {
		if (p[i]>=48&&p[i]<=57)
		{
			num[count_num++] = p[i];
		}
		else if ((p[i]>=65&&p[i]<=90)||(p[i]>=97&&p[i]<=122))
		{
			alp[count_alp++] = p[i];
		}
	}
	free(p);
	p = NULL;
	printf("字母部分为：");
	for ( i = 0; i < count_alp; i++)
	{
		printf("%c", alp[i]);
	}
	printf("\n");
	printf("数字部分为：");
	for (i = 0; i < count_num; i++)
	{
		printf("%c", num[i]);
	}
}

//将 字 符 串 中 的 空 格 替 换 成 “ % 020” ， 例 如 “hello world how ”
//->”hello % 020 % 020 % 020world % 020 % 020 % 020how % 020 % 020 % 020 % 020”
void replacespace() {
	printf("请输入字符串：");
	char *p,*q;
	char s[len_default];
	gets(s);
	p = (char*)malloc(strlen(s) + 1);
	strcpy(p, s);
	int i,j;
	q = (char*)malloc(len_default);
	for ( i = 0,j=0; i <= strlen(p); i++)
	{
		if (p[i] == 32) {
			q[j++] = '%';
			q[j++] = '0';
			q[j++] = '2';
			q[j++] = '0';
			
	}
		else
		{
		q[j++] = p[i];
		}
	}puts(q);
}
//3.删除字符串中指定的字符。 例如 “abcdaefaghiagkl“ 删除‘a’, 以后： “bcdefghigkl”
//思路：遍历，遇到a字母就去除，然后打印，难的地方在于直接对当前空间的a字符进行删除，后面的元素往前移动，来解决这个问题。
void deletechar(char *s, char ch) {
	char *t = s;//目标指针先指向原字符头部
	while (*s!='\0')//遍历字符串s
	{
		if (*s != ch) {
			*t = *s;
			t++;//每放置一个字符，t就移动一个位置
		}s++;//检查下一字符
	}
	*t='\0';

}
//4.删除一个有序数组中重复的元素。例如 1, 2, 2, 2, 3, 3, 3, 4, 4, 5, 5, 5, 6, 6, 6 -> 1, 2, 3, 4, 5, 6
#define E -858993460
void deleterepeatednumber(int *p) {
	int *q = p;
	int *s= p;
	int *r;
	p++;
	
	while (*p!=E)
	{
		int n = 1;
		for (r=s; r <= q; r++)
		{
			if (*p == *r)
			{
				n=0;
			}
			
		}
		if(n==1){ *++q = *p; }
		
		p++;
	}
	*(q+1) =E;
}

//将 字 符 串 中 的 相 邻 的 多 余 空 格 去 掉 ， 例 如 （空 格 用 下 划 线 表示）： ”___hello____world___how_are_you__”->”hello_world_how_are_you”
//思路：通过遍历来解决即可，判断和前一个元素一样，并且都是空格，那么就不存入到新数组，否则存入，最后结果就是只有一个空格的字符串。
void deletespace(char* s) {
	char *t = s;//目标指针先指向原字符头部
	while (*s != '\0')//遍历字符串s
	{
		if (*s != ' '||(*s==' '&&*(s-1)!=' ')) {
			*t = *s;
			t++;//每放置一个字符，t就移动一个位置
		}s++;//检查下一字符
	}
	*t = '\0';

}
//
//求一个字符串数组的最大值和次大值  void big(char *arr[], int size, char** big11, char** big22)
//思路：在main函数定义字符指针数组char*  arr[5] = { “wangdao”,”fenghua”,”xianyu”,”loulou”,”longge” } ，
//还有char* big1, char* big2，把&big1和&big2传递给big函数，最终big函数执行结束后，
//big1指向最大字符串，big2指向次大字符串
void big(char *arr[],int n, char** big11, char** big22) {
	int i, j;
	char* t;
	char **p2=arr;
	for (i = 0; i < n; i++) {
		for ( j = 0; j < n-i-1; j++)
		{
			if (strcmp(p2[j], p2[j + 1]) == 1) {
				t = p2[j + 1];
				p2[j + 1] = p2[j];
				p2[j] = t;
			}
		}
	}
	*big11 = p2[n - 1];
	*big22 = p2[n - 2];
}


int main() {
	//distribution();
	//replacespace();
	//char a[]="hello world  how  are    U ";
	//char *p = a;
	//deletespace(p);
	//deleterepeatednumber(a);
	//puts(p);
	char*  arr[6];
	char b[6][10]= { "Camille","Fiora","Irelia","Riven","Akali","Gwen" };
	int i;
	for (i = 0; i < 6; i++) {
		arr[i] = b[i];
	}
	char* big1 = arr[0];
	char* big2 = arr[1];
	big(arr, 6, &big1, &big2);
	//char** big1, **big2;
	//big(arr, 6, big1, big2);
	printf("该字符串的最大值为：%s", big1);
	printf("\n");
	printf("该字符串的次大值为：%s", big2);
	printf("\n");
	for (i = 0; i < 6; i++) {
		printf("%s\n", b[i]);
	}
}